﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    class Program
    {
        private Program()
        {

        }
        static void Main(string[] args)
        {
            
            Book book1 = new Book("Kites", 11, "Vasia Shew");
            Book book2 = new Book("alpha", 223, "Vasia Shew");
            Book book4 = new Book("Reconstrruction", 129, "Vasia Shewaaa");
            Book book3 = new Book("Sunni", 121, "Vasia Shewaaa");
            Book book5 = new Book("Tokiore", 222, "Vasia Shew");
            Book book6 = new Book("Religions", 343, "Vasia Sheg");
            Book book7 = new Book("Related", 429, "Vasia HE");
            Book book8 = new Book("elpha", 329, "Vasia HE");

            Department department1 = new Department("Fictional");

            department1.BooksInDepartment.Add(book1);
            department1.BooksInDepartment.Add(book2);
            department1.BooksInDepartment.Add(book3);
            department1.BooksInDepartment.Add(book4);
            department1.BooksInDepartment.Add(book5);
            department1.BooksInDepartment.Add(book6);
            department1.BooksInDepartment.Add(book7);
            department1.BooksInDepartment.Add(book8);
            
            Department department2 = new Department("IT");

            Book book9 = new Book(".Net", 129, "Shild");
            Book book10 = new Book("C#", 129, "Microsoft");

            department2.BooksInDepartment.Add(book9);
            department2.BooksInDepartment.Add(book10);

            Library library = new Library("Franko Library");

            library.listOfDepartments.Add(department1);
            library.listOfDepartments.Add(department2);
          
            //interface methods
            ICountingBooks countingBooksInlibrary = library as ICountingBooks;
            Console.WriteLine(countingBooksInlibrary.GetNumbOfBooksInLibrary());
            Console.WriteLine(library.ToString());

            //additional methods
            List<Author> listOfAuthor = new List<Author>();
            listOfAuthor = countingBooksInlibrary.GetAuthorsAndNumbOfAuthorsBooks();
            
            Author authorWithMostBooks = AdditionalMethods.AuthorWithMostBooks(listOfAuthor);
            Console.WriteLine(authorWithMostBooks.ToString());

           

            UsingJSON.Ser(library);
            UsingJSON.DeSerialize();
            Console.ReadLine();
        }
    }
}
