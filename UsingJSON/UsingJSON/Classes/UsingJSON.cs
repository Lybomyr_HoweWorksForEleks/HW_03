﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication1
{
    static class UsingJSON
    {
        public static void Serialize(Library library)
        {
            JObject newLibrary = new JObject();
            JProperty libraryName = new JProperty("Library Name", library.NameOfLibrary);
            JProperty departments = null;

            newLibrary.Add(libraryName);
            foreach (Department department in library.listOfDepartments)
            {
                JObject newDepartment = new JObject();
                JProperty departmentName = new JProperty("Department Name", department.Name);
                JProperty numbOfBooks = new JProperty("Number of books", department.BooksInDepartment.Count);
                newDepartment.Add(departmentName);
                newDepartment.Add(numbOfBooks);
                JProperty booksInDepartment = null;
                
                foreach (Book book in department.BooksInDepartment)
                {
                    JObject newBook = new JObject();
                    JProperty bookName = new JProperty("Book Name", book.BookName);
                    JProperty authorName = new JProperty("Author Name", book.AuthorName);
                    JProperty numbOfPages = new JProperty("Numb of pages", book.NumbOfPages);

                    newBook.Add(bookName);
                    newBook.Add(authorName);
                    newBook.Add(numbOfPages);
                    booksInDepartment = new JProperty(book.BookName, newBook);
                    newDepartment.Add(booksInDepartment);
                }
                
                departments = new JProperty(department.Name, newDepartment);

                newLibrary.Add(departments);
            }
             
            string json = newLibrary.ToString();
            StreamWriter sw = new StreamWriter("SerializeToJSON.json");
            sw.Write(json);
            sw.Close();
        }

        public static void Ser(Library library)
        {
           string s =  JsonConvert.SerializeObject(library);
           StreamWriter write = new StreamWriter("ser.json");
            write.WriteLine(s);
            write.Close();
        }

        public static Library DeSerialize()
        {
            string lib = File.ReadAllText("ser.json");
            Library library = JsonConvert.DeserializeObject<Library>(lib);
            return library;
        }


    }
}
