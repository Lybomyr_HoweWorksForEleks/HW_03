﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication1
{
    static class AdditionalMethods
    {
        public static Author AuthorWithMostBooks(List<Author> listAuthor)
        {
            Author resultAuthor;
            Author[] sortAuthor = new Author[listAuthor.Count];
            listAuthor.CopyTo(sortAuthor);
            Array.Sort(sortAuthor);
            resultAuthor = sortAuthor[listAuthor.Count - 1];
            return resultAuthor;
        }

        public static Book BookWithMostPages(List<Book> listBooks)
        {
            Book resultBook;
            Book[] sortBooks = new Book[listBooks.Count];
            listBooks.CopyTo(sortBooks);
            Array.Sort(sortBooks);
            resultBook = sortBooks[0];
            return resultBook;
        }

        public static Department DepartmentWithMostBooks(List<Department> listDepartments)
        {
            Department resultDepartment;
            Department[] sortDepartment = new Department[listDepartments.Count];
            listDepartments.CopyTo(sortDepartment);
            Array.Sort(sortDepartment);
            resultDepartment = sortDepartment[listDepartments.Count - 1];
            return resultDepartment;
        }
    }
}
