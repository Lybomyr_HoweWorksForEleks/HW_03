﻿using System.Collections.Generic;

namespace ConsoleApplication1
{
    class Library : ICountingBooks
    {
        int numbOfBooksInLibrary;
        public List<Department> listOfDepartments = new List<Department>();
        
        public string NameOfLibrary { get; private set; }
        public Library(string name)
        {
            this.NameOfLibrary = name;
        }

        int ICountingBooks.GetNumbOfBooksInLibrary()
        {
            for (int i = 0; i < listOfDepartments.Count; i++)
            {
                numbOfBooksInLibrary += listOfDepartments[i].BooksInDepartment.Count;
            }
            return numbOfBooksInLibrary;
        }

        List<Author> ICountingBooks.GetAuthorsAndNumbOfAuthorsBooks()
        {
            List<string> listOfAuthors = new List<string>();
            List<Author> informationAboutAuthors = new List<Author>();
            
            for (int i = 0; i < listOfDepartments.Count; i++)
            {
                for (int j = 0; j < listOfDepartments[i].BooksInDepartment.Count; j++)
                {
                    listOfAuthors.Add(listOfDepartments[i].BooksInDepartment[j].AuthorName);
                }
            }
            string[] copyList = new string[listOfAuthors.Count];
            listOfAuthors.CopyTo(copyList);
            for (int i = 0; i < listOfAuthors.Count; i++)
            {
                Author newAuthor = new Author(listOfAuthors[i]);
                
                if (listOfAuthors[i] != "")
                {
                    for (int j = i; j < listOfAuthors.Count; j++)
                    {
                        if (listOfAuthors[i] == copyList[j])
                        {
                            newAuthor.numbOfAuthorBooks++;
                            copyList[j] = "";
                        }
                    }

                    if (newAuthor.numbOfAuthorBooks >  0)
                    {
                        informationAboutAuthors.Add(newAuthor);
                        
                    }
                    listOfAuthors[i] = "";
                }
            }
            return informationAboutAuthors;
        }

        List<Department> ICountingBooks.GetNumbOfBooksInDepartment()
        {
            List<Department> informationAboutDepartments = new List<Department>();
            for (int i = 0; i < listOfDepartments.Count; i++)
            {
                informationAboutDepartments[i].Name = this.listOfDepartments[i].Name;
                informationAboutDepartments[i].BooksInDepartment = this.listOfDepartments[i].BooksInDepartment;
            }
            return informationAboutDepartments;
        }

        public override string ToString()
        {
            return "Library name - " + this.NameOfLibrary +
                " |Number of books in library - " + this.numbOfBooksInLibrary;
        }
    }
}
