﻿using System;

namespace ConsoleApplication1
{
    class Book : IComparable
    {
        public string BookName { get; set; }
        public int NumbOfPages { get; set; }
        public string AuthorName { get; set; }

        public Book(string name, int numbOfPages, string authorName)
        {
            this.BookName = name;
            this.NumbOfPages = numbOfPages;
            this.AuthorName = authorName;
        }

        int IComparable.CompareTo(object obj)
        {
            Book item = obj as Book;
            if (item != null)
            {
                return this.NumbOfPages.CompareTo(item.NumbOfPages);
            }
            else
            {
                throw new ArgumentException("Argument is not Book!");
            }
        }

        public override string ToString()
        {
            return "Book name - " + this.BookName +
                " |Author name - " + this.AuthorName +
                " |Number of pages - " + this.NumbOfPages;
        }
    }
}
