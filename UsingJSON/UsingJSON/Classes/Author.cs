﻿using System;

namespace ConsoleApplication1
{
    class Author : IComparable

    {
        public string AuthorName { get; set; }
        public int numbOfAuthorBooks = 0;

        public Author(string authorName)
        {
            this.AuthorName = authorName;
        }

        int IComparable.CompareTo(object obj)
        {
            Author item = obj as Author;
            if (item != null)
            {
                return this.numbOfAuthorBooks.CompareTo(item.numbOfAuthorBooks);
            }
            else
            {
                throw new ArgumentException("Argument is not Author!");
            }

        }
        
        public override string ToString()
        {
            return "Author name - " + this.AuthorName + " |Number of author books - " + this.numbOfAuthorBooks;
        }
    }
}
